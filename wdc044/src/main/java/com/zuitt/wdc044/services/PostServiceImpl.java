package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepositories;
import com.zuitt.wdc044.repositories.UserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepositories postRepositories;

    @Autowired
    private UserRepositories userRepositories;

    @Autowired
    JwtToken jwtToken;

    public void createPost(String stringToken, Post post){
        User author = userRepositories.findByUsername(jwtToken.getUsernameFromToken(stringToken));


        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());

        newPost.setUser(author);

        postRepositories.save(newPost);
    }

    public Iterable<Post> getPosts(){
        return postRepositories.findAll();
    }
}
