package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PostRepositories extends CrudRepository<Post, Object> {

}
