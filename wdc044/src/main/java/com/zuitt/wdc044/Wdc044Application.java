package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


//this is a shortcut or method we manually create or describe our purpose of configure a framework

@SpringBootApplication

@RestController
public class Wdc044Application {
	//This method
	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);

	}


	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World")String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World")String greet){
		return String.format("Good evening, %s! Welcome to Batch 291!", greet );
	}

    //Activity 1
    @GetMapping("/hi")
    public String hi(@RequestParam(value = "user", defaultValue = "World")String hi){
        return String.format("Hi %s", hi);
    }

}
